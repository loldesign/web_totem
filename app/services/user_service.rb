#encoding: utf-8
module UserService
  extend self

  def find(cpf)
    operator = find_operator(cpf)

    if not_found?(operator)
      find_cyclist(cpf)
    else
      operator
    end
    
  rescue Errno::ECONNREFUSED
    connection_error
  rescue SocketError
    internet_error
  end

  private

  def connection_error
    OpenStruct.new(valid: false, reason: 'connection_error', message: 'Erro de conexão. Tente novamente mais tarde.')
  end

  def internet_error
    OpenStruct.new(valid: false, reason: 'internet_error', message: 'Sem internet. Tente mais tarde')
  end

  def not_found?(user)
    user.reason == 'not_found'
  end

  def find_cyclist(cpf)
    finder('Cyclist', cpf)
  end

  def find_operator(cpf)
    finder('Mechanical', cpf)
  end

  def manager_for(type)
    "CompartbikeApi::#{type}Manager".constantize
  end

  def finder(type, cpf)
    clean_cpf = cpf.gsub(/[.-]/,'')
    response = manager_for(type).new(clean_cpf).json

    OpenStruct.new(response)
  end
end