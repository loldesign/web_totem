class RemoteResource < ActiveResource::Base
  self.site = Settings.server.path
  self.format = :json
end