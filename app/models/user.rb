class User < RemoteResource

  def self.find_by_cpf(cpf)
    find(:one, from: "/#{self.to_s.downcase << 's'}/cpf.json?cpf=#{cpf}")
  rescue ArgumentError
    nil
  rescue
    :connection_error
  end
end