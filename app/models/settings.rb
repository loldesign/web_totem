class Settings < Settingslogic
  source "#{Rails.root}/config/compartbike_api.yml"
  namespace Rails.env
end