module ApplicationHelper
  def title(text)
    content_for(:title){ text }
  end

  def has_bike?(slot)
    slot.state == "busy" && slot.sensor_status == "has_bike"
  end

  def user_slot_number slot
    fix_slot_number slot, "-"
  end

  def api_slot_number slot
    fix_slot_number slot, "+"
  end

  private

  # BUG: slot 10 and 17 are always broken,
  # so we skip the number 10 and 17 to 11 and 18
  def fix_slot_number(slot, method)
    slot = slot.send(method.to_sym, 1) if slot >= 10
    slot = slot.send(method.to_sym, 1) if slot >= 17
    slot
  end
end
