# encoding: UTF-8
class SlotsController < ApplicationController
  def select
    increment_tries!

    @slots = CompartbikeApi::Slot.order_by(:name.asc)
  end

  def release_bike
    slot_name        = view_context.api_slot_number(params[:slot_number].to_i)
    @slot            = CompartbikeApi::Slot.where(name: slot_name).first
    @role            = session[:is_mechanical?] ? 'mechanical' : 'cyclist'
    @release_manager = CompartbikeApi::ReleaseManager.new(@slot, session[:cpf], @role)

    if @release_manager.create_release
      more_options_or_releasing_path
    else
      redirect_to_error_or_exceeded
    end
  rescue
    redirect_to_error_or_exceeded
  end

  private
  def redirect_to_error_or_exceeded
    if exceeded_tries?
      redirect_to(root_path, notice: 'Tentativas Excedidas')
    else
      error "Baía inválida. Selecione outra baia."
    end
  end

  def more_options_or_releasing_path
    if session[:is_mechanical?]
      redirect_to select_slot_path, notice: 'Retire Outra Bicicleta ou Espere voltar para Página Inicial'
    else      
      render :releasing_bike
    end
  end
end
