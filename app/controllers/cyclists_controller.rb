# encoding: UTF-8
class CyclistsController < ApplicationController

  def find
    user = UserService.find(params[:cpf])

    if user.valid
      session[:login]          = user.login
      session[:cpf]            = user.cpf
      session[:is_mechanical?] = user.type == 'Mechanical'
      redirect_to password_path
    else
      redirect_to root_path, notice: user.message
    end
  end

  def password
    increment_tries!
  end

  def authenticate
    login = session[:login]
    password = params[:password]
    session = UserSession.authenticate(login, password)
    if session != :connection_error && session.persisted?
      reset_tries!

      redirect_to controller: :slots, action: :select
    elsif session == :connection_error
      error 'Erro de conexão. Tente novamente mais tarde.'
    else

      if exceeded_tries?
        redirect_to(root_path, notice: 'Tentativas Excedidas')
      else
        error 'Senha Invalida. Tente novamente!'
      end

    end
  end
end