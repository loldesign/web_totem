#encoding: utf-8
class ApplicationController < ActionController::Base
  protect_from_forgery

  protected
    def error(message)
      flash[:error] = message
      redirect_to :back
    end

    def exceeded_tries?
      session[:tries] >= 3
    end

    def reset_tries!
      session[:tries] = 0
    end

    def increment_tries!
      session[:tries] += 1
    end
end
