# encoding: UTF-8
class StationsController < ApplicationController
  respond_to :json, :html

  def index
    @stations = Station.all

    respond_with(@stations)
  end
end
