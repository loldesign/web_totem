# encoding: UTF-8
class HomeController < ApplicationController
  def index
    reset_tries!
    session[:login] 		 = nil
    session[:is_mechanical?] = nil
    flash[:notice]	= "Tempo de sessão esgotado. Retornando ao ínicio" if params[:timeout] && params[:timeout]=true
  end
end
