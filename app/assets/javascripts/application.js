// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

$(function(){
  ignore_key = function (key_value){
    var ignored_keys = [60, 62, 91, 93, 123, 125];
    for(n in ignored_keys)
      if(ignored_keys[n] === key_value)
        return true;
    return false;
  }

  $(document).keypress(function (e){
    if(ignore_key(e.which)){
      // alert("Pressionou "+String.fromCharCode(e.which));
      if(e.which === 91){
        input = $("input[type=text], input[type=password]").first();
        value = input.val();
        input.val( value.substr(0, value.length - 1) );
      }
      e.preventDefault();
    }
  });

  var time_without_input = 0;

  $("#cyclist_cpf").mask("999.999.999-99");

  function increase_time_without_input(){
    if(window.location.pathname != "/"){
      var timeOut = $("[data-timeout]").attr('data-timeout') || 30;

      if(time_without_input >= parseInt(timeOut)){
        window.location = "/?timeout=true";
      }
      time_without_input = time_without_input + 1;
      setTimeout(increase_time_without_input, 1000);
    }
  }

  setTimeout(increase_time_without_input, 1000);

  setTimeout(function(){
    $(".notice, .alert, .error").fadeOut();
  }, 5000);

  needToCheckEnterBtn();
 
  function needToCheckEnterBtn(){
    var message_dom = $("[data-need-to-check-pressed-enter='true']");

    if(message_dom[0]){
      $(document).keypress(function(e) {
        if(e.which == 13) {
          showAlertAfeterPressEnter(message_dom);
        }
      });
    }
  }


  function showAlertAfeterPressEnter(message_dom){
    message_dom.append(message_dom.attr('data-message'));
    message_dom.show();
  }
});
