//KeyCode 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
var keyCodeList = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57]

$(document).ready(function() {
  var $map = $('#map_canvas');
  if($map[0]){
    new mapManager($map);
  }
});

var mapManager = function(obj){
  var that     = this;
  this.map     = obj;
  this.mapObj  = undefined;
  this.zoom    = 14;
  this.options = {
    zoom: that.zoom,
    center: new google.maps.LatLng(-23.797,-46.024),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  this.markers = undefined;

  this.startup = function(){
    this.createMap();
    this.markers = new markerManager(this.mapObj);

    $(document).keyup(function(e) {
      if(e.keyCode == 65){
        that.add_zoom();
      }else if(e.keyCode == 66){
        that.remove_zoom();
      }     
    });
  },

  this.createMap = function(){
    this.mapObj = new google.maps.Map(this.map[0], this.options);
  },

  this.add_zoom = function(){
    if( (this.zoom += 1) <= 20){
      this.mapObj.setZoom(this.zoom);
    }else{
      this.zoom = 20;
    }
  },

  this.remove_zoom = function(){
    if( (this.zoom -= 1) > 0){
      this.mapObj.setZoom(this.zoom);
    }else{
      this.zoom = 1;
    }
  }

  this.startup();
}

var markerManager = function(map){
  var that        = this;
  this.infobubble = new InfoBubble();
  this.map        = map;
  this.markers    = [];

  this.loadMarkers = function(){
    jQuery.get("/stations.json", function(stations){
      jQuery.each(stations, function(index, station){
        that.addMarker(station);
      });
    });
  },

  this.addMarker = function(station){
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(station.googleMapX, station.googleMapY),
      title: station.name,
      id: station.id,
      station_number: station.station_number,
      icon: "/google-maps-utility/images/map_bike_icon.png",
      status: station.status_to_human,
      available_slots_size: station.available_slots_size,
      unavailable_slots_size: station.unavailable_slots_size,
      map: that.map
    });

    this.addClickListenerMarker(marker);

    this.markers[station.station_number] = marker;
  },

  this.addClickListenerMarker = function(marker){
    google.maps.event.addListener(marker, 'click', function () {
      that.openMarker(marker);
    });
  },

  this.openMarker = function(marker){
    bubble_content = '<div class="info_item">'+marker.station_number+' - '+marker.title+'<br>Status: '+marker.status+'<br>Bicicletas Disponíveis: '+marker.unavailable_slots_size+'<br>Baias Disponíveis: '+marker.available_slots_size+'</div>'
      that.infobubble.close();
      that.infobubble = new InfoBubble({
        borderRadius: 12,
        padding: 15,
        arrowSize: 10,
        maxWidth: 260,
        arrowStyle: 2,
        content: bubble_content
      });
      that.infobubble.open(map, marker);
  }

  this.startup = function(){
    this.loadMarkers();

    $(document).keyup(function(e) {
      if(keyCodeList.indexOf(e.keyCode) !== -1){
        var marker = that.markers[keyCodeList.indexOf(e.keyCode)];
        that.openMarker(marker);
      }else if(e.keyCode === 27){
        that.infobubble.close();
      } 
    });    
  }

  this.startup();
}