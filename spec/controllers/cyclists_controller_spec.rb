require 'spec_helper'

describe CyclistsController do
  describe 'GET "find"' do
    let(:cyclist)    {'35277745807'}
    let(:cyclist_invalid) {'33389842861'}
    let(:mechanical) {'51631585118'}
    let(:not_found)  {'82727266654'}

    context 'when is cyclists' do
      context 'when is valid' do
        use_vcr_cassette "login_cyclist"
        before{ get :find, cpf: cyclist }

        it{ session[:cpf].should eq('352.777.458-07') }
        it{ session[:login].should eq('352.777.458-07') }
        it{ session[:is_mechanical?].should be_false }

        it{ should redirect_to(password_path) }
        it{ flash.should be_empty }
      end

      context 'when is not_found' do
        use_vcr_cassette "login_not_found"
        before{ get :find, cpf: not_found }

        it{ should redirect_to(root_path) }
        it{ flash[:notice].should eq('Usuário não encontrado. Tente novamente!')}
      end

      context 'when is invalid' do
        use_vcr_cassette "login_invalid"
        before{ get :find, cpf: cyclist_invalid }

        it{ should redirect_to(root_path) }
        it{ flash[:notice].should_not be_empty}
      end

      context 'when raise connection error' do
        use_vcr_cassette "no_internet"
        before{ get :find, cpf: cyclist }

        it{ should redirect_to(root_path) }
        it{ flash[:notice].should eq('Erro de conexão. Tente novamente mais tarde.')}
      end
    end

    context 'when is mechanical' do
      context 'when is valid' do
        use_vcr_cassette "login_mechanical"
        before{ get :find, cpf: mechanical }

        it{ session[:cpf].should eq('516.315.851-18') }
        it{ session[:login].should eq('Eduardo') }
        it{ session[:is_mechanical?].should be_true }

        it{ should redirect_to(password_path) }
        it{ flash.should be_empty }
      end
    end
  end
end