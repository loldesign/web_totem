require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/whenever'

require_relative 'custom_deploy'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :deploy_to, '/var/www/web_totem'
set :repository, 'https://bitbucket.org/loldesign/web_totem.git'
set :branch, 'master'

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/mongoid.yml', 'config/compartbike_api.yml', 'config/keep_alive.yml', 'log']

# Optional settings:
set :user, 'compartibike'    # Username in the server to SSH to.
set :term_mode, nil
#   set :port, '30000'     # SSH port number.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use[ruby-1.9.3-p125@default]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[touch "#{deploy_to}/shared/config/mongoid.yml"]
  queue! %[touch "#{deploy_to}/shared/config/compartbike_api.yml"]
  queue! %[touch "#{deploy_to}/shared/config/keep_alive.yml"]
  queue! %[touch "#{deploy_to}/shared/config/nginx.conf"]

  queue! %[ln -sf "#{deploy_to}/shared/config/nginx.conf" /opt/nginx/sites-enabled/web_totem]

  queue  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]
  queue  %[echo "-----> Be sure to edit 'shared/config/compartbike_api.yml'."]
  queue  %[echo "-----> Be sure to edit 'shared/config/keep_alive.yml'."]
  queue  %[echo "-----> Be sure to edit 'shared/config/nginx.conf'."]
end

desc "Deploy via hamachi network."
task :hamachi_deploy => :environment do
  CustomDeploy.new(self, ENV['to']).deploy
end

desc "Setup via hamachi network."
task :hamachi_setup => :environment do
  CustomDeploy.new(self, ENV['to']).setup
end

desc "Deploys the current version to the server."
task :deploy => :environment do

  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    # invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'

    to :launch do
      queue "mkdir -p #{deploy_to}/current/tmp"
      queue "touch #{deploy_to}/current/tmp/restart.txt"
      invoke :'whenever:clear'
      invoke :'whenever:update'
    end
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers