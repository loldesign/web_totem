env :PATH, ENV['PATH']

set :bundle_command, "/usr/local/bin/bundle exec"

every "*/1 * * * *" do
  rake 'keep_alive:notify'
end
