require "resque/server"
require "resque-retry"
require "resque-retry/server"

WebTotem::Application.routes.draw do
  match "/find_cyclist"   => "cyclists#find",         as: :find_cyclist
  match "/authenticate"   => "cyclists#authenticate", as: :authenticate_cyclist
  match "/password"       => "cyclists#password",     as: :password
  match "/select_slot"    => "slots#select",          as: :select_slot
  match "/release_bike"   => "slots#release_bike",    as: :release_bike
  match "/releasing_bike" => "slots#releasing_bike"
  get   "/stations"       => 'stations#index'

  mount CompartbikeApi::Engine => "/"
  mount Resque::Server.new, at: "/resque"
  root to: "home#index"
end
