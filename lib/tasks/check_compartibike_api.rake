namespace :check_compartibike_api do

  desc 'Check workers and failed process'
  task go: :environment do
    CompartbikeApi::Resque.process
  end
end
