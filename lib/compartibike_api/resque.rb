module CompartbikeApi::Resque
  @@value_state = 0
  @@value_command = 0
  @@count_state = 0
  @@count_command = 0
  def self.count_jobs(queue_name)
    Resque.peek(queue_name, 1, 50).count
  end

  def self.count_workers
     Resque::Worker.all.count
  end

  def self.process
    while true do
      job_commads = count_jobs('commands')
      job_state = count_jobs('update_state')
      workers = count_workers

      puts "---> QUEUE commands has #{job_commads} jobs"
      puts "---> QUEUE commands has #{job_state} jobs"
      puts "---> has #{count_workers} WORKERS"
      
      reset_state(job_state)
      reset_commands(job_commads)
      clear_worker(count_workers)

      sleep 30
    end
  end

  def self.reset_commands(count)
    if count != @@value_command
      @@value_state = count
      @@count_command = 0
    else
      @@count_command += 1
    end
    if @@count_command > 4
      puts "---> Fila command travada por mais de 2,5 minuto"
      my_logger ||= Logger.new("#{Rails.root}/log/command.log")
      my_logger.info("---> Fila command travada por mais de 2,5 minuto")
    end
  end

  def self.reset_state(count)
    if count != @@value_command
      @@value_state = count
      @@count_command = 0
    else
      @@count_command += 1
    end
    if @@count_command > 4
      puts "---> Fila state travada por mais de 2,5 minuto"
      my_logger ||= Logger.new("#{Rails.root}/log/state.log")
      my_logger.info("---> Fila state travada por mais de 2,5 minuto")
    end
  end

  def self.clear_worker(count)
    if count > 2 
      puts "---> Existem mais do que 2 workers"
      my_logger ||= Logger.new("#{Rails.root}/log/workers.log")
      my_logger.info("---> Existem mais do que 2 workers")
      sleep 1.2
      `echo FLUSHALL | redis-cli`
      sleep 1.2
      `sudo reboot now`
    end
  end
end

#save at: /lib/compartibike_api/resque.rb

#how to create rake task
# http://railsguides.net/how-to-generate-rake-task/

# redis command example
# Resque.flush_all

# reference pages
# https://github.com/resque/resque/tree/master/lib/resque/server/views
